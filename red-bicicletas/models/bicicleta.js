var Bicicleta = function(id, color, modelo, ubicacion) {
	this.id = id;
	this.color = color;
	this.modelo = modelo;
	this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
	return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici) {
	Bicicleta.allBicis.push(aBici);
}

Bicicleta.removedById = function(aBiciId) {
	for(var i = 0; i < Bicicleta.allBicis.length; i++) {
		if(Bicicleta.allBicis[i].id = aBiciId) {
			Bicicleta.allBicis.splice(i, 1);
			break;
		}
	}
}

Bicicleta.findById = function(aBiciId) {
	var aBici = Bicicleta.allBicis.find(x=> x.id == aBiciId);
	if(aBici)
		return aBici;
	else
		throw new Error(`No existe una bicicleta con el id ${aBici}`);
}

/*
var a = new Bicicleta(1, 'rojo', 'urbana', [-31.326298, -64.262076]);

Bicicleta.add(a);
*/

module.exports = Bicicleta;