var Bicicleta = require('../../models/bicicleta');

beforeEach(() => { Bicicleta.allBicis = []; });

describe('Bicicleta.allBicis', () => {
	// Que quiero probar
	it('comienza vacia', () => {
		// Que esperamos
		expect(Bicicleta.allBicis.length).toBe(0);
	});
});

describe('Bicicleta.add', () => {
	it('agregamos una', () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var a = new Bicicleta(1, 'rojo', 'urbana', [-31.326298, -64.262076]);
		Bicicleta.add(a);
		expect(Bicicleta.allBicis.length).toBe(1);
		expect(Bicicleta.allBicis[0]).toBe(a);
	});
});

describe('Bicicleta.findById', () => {
	it('Debe devovler la bici con id 1', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
		var aBici = new Bicicleta(1, "Verde", "Urbana");
		var aBici2 = new Bicicleta(2, "Rojo", "Competicion");
		Bicicleta.add(aBici);
		Bicicleta.add(aBici2);

		var targetBici = Bicicleta.findById(1);
		expect(targetBici.id).toBe(1);
		expect(targetBici.color).toBe(aBici.color);
		expect(targetBici.modelo).toBe(aBici.modelo);
	});
});

describe('Bicicleta.removedById', () => {
	it('Eliminamos la bici con id 1', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
		
		var aBici = new Bicicleta(1, "Verde", "Urbana");
		var aBici2 = new Bicicleta(2, "Rojo", "Competicion");
		Bicicleta.add(aBici);
		Bicicleta.add(aBici2);
		
		expect(Bicicleta.allBicis.length).toBe(2);

		Bicicleta.removedById(1);
		
		expect(Bicicleta.allBicis.length).toBe(1);
	});
});